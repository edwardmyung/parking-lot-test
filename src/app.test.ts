const request = require('supertest')
import {app} from './app'

describe('POST /new', () => {
  it('should take a post request', async done => {
    const VEHICLE_PLATE_NUM = 'DA53 URZ'
    const VEHICLE_MAKE = 'Ford'
    const VEHICLE_COLOR = 'Green'

    const res = await request(app).post('/new').type('json').send({
      vehicle_plate_num: VEHICLE_PLATE_NUM,
      vehicle_make: VEHICLE_MAKE,
      vehicle_color: VEHICLE_COLOR
    })

    expect(res.body.vehicle_plate_num).toEqual(VEHICLE_PLATE_NUM)
    done()
  })

  it('should reject a post request for a car that is already in the lot', async done => {
    const REQUEST = {
      vehicle_plate_num: 'DA53 URZ',
      vehicle_make: 'Ford',
      vehicle_color: 'Green'
    }
    await request(app).post('/new').type('json').send(REQUEST)

    const res = await request(app).post('/new').type('json').send(REQUEST)
    expect(res.status).toEqual(400)
    done()
  })
})

describe('GET index', () => {
  it('should work', async () => {
    const res = await request(app).get('/')

    expect(res.body).toHaveProperty('foo')
    expect(res.body.foo).toEqual('bar')
  })
})
