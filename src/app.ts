import express from 'express'
import bodyParser from 'body-parser'

const app = express()

const db : {
  vehicles : {
    [vehicle_plate_num: string] : {
      vehicle_plate_num: string,
      vehicle_make: string,
      vehicle_color: string
    }
  }
  entries: {
    [vehicle_plate_num: string] : {
      entry_time: number,
      exit_time?: number
    }
  }
} = {
  vehicles: {},
  entries: {}
}

// Enable JSON parsing on request.body
app.use(bodyParser.json())

app.post('/new', (request, response) => {
  const {body} = request

  const entry = db.entries[body.vehicle_plate_num]
  if(entry && !entry.exit_time) {
    response.status(400).send("This vehicle is already in the parking lot")
  }

  db.vehicles[body.vehicle_plate_num] = body
  db.entries[body.vehicle_plate_num] = {
    entry_time: new Date().getTime() / 1000
  }

  response.json({vehicle_plate_num : body.vehicle_plate_num})
})

app.get('/', (request, response) => {
  response.json({foo: 'bar'})
})

export {app}
